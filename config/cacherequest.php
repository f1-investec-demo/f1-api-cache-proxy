<?php

return [
    // Cache Time to Live (defaults to 14 days)
    'ttl' => env('CACHE_TTL', 1209600)
];
