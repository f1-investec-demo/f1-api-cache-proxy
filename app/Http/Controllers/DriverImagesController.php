<?php

namespace App\Http\Controllers;

use App\Http\Resources\DriverImageCollection;
use App\Models\DriverImage;
use Illuminate\Http\Request;

class DriverImagesController extends Controller
{
    public function index(Request $request): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $familyName = $request->get('family-name');
        $givenName = $request->get('given-name');

        $driverImage = DriverImage::when($familyName, function ($query, $familyName)  {
            $query->where('family_name', $familyName);
        })
            ->when($givenName, function($query, $givenName) {
                $query->where('given_name', $givenName);
            })->get();

        return DriverImageCollection::collection($driverImage);
    }
}
