<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Support\Facades\Cache;

class CountryController extends Controller
{
    function index() {

        if(! Cache::has('country:all')) {
            $data = Country::all();
            Cache::put('country:all', $data);

            return $data;
        }

        return Cache::get('country:all');
    }

    public function show($country) {

        $countryAlternativeMap = [
            'UK'        => 'United Kingdom',
            'UAE'       => 'United Arab Emirates',
            'Korea'     => 'Korea, Republic of',
            'USA'       => 'United States',
            'Russia'    => 'Russian Federation',
        ];

        $country = $countryAlternativeMap[$country] ?? $country;

        if(! Cache::has("country:$country")) {
            $data = Country::where('name', $country)->firstOrFail();
            Cache::put("country:$country", $data);
            return $data;
        }

        return Cache::get("country:$country");

    }
}
