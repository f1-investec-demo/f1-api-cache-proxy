<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Request as RequestDB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Str;

class RequestController extends Controller
{

    private function url($part, $limit = 30, $offset = 0) {
        $part = str_replace(['.json'], '' ,$part);
        return "https://ergast.com/api/f1/$part.json?limit={$limit}&offset={$offset}";
    }

    private function buildSegments($args) {
        array_shift($args);

        return implode('/', $args);
    }

    private function requestAndStore($url) {
        $response = Http::get($url);

        $requestdb = new RequestDB();
        $requestdb->url = $url;
        $requestdb->type = 'json';
        $requestdb->file_name = Str::random(64) . '.json';
        $requestdb->save();

        Storage::put($requestdb->file_name, $response->body());

        return $requestdb;
    }

    public function fetch(Request $request) {
        $segments = $this->buildSegments(func_get_args());

            $ttlStart = now()->subSeconds(config('cacherequest.ttl'));
            $ttlEnd = now();

            $limit = $request->get('limit', 30);
            $offset = $request->get('offset', 0);

            $url = $this->url( $segments, $limit,$offset);

            if(Cache::has($url)) {
                return Cache::get($url);
            }

            $requestDb = RequestDB::where('url', $url)->whereBetween('created_at', [$ttlStart, $ttlEnd])->orderBy('created_at', 'DESC')->first();

            if($requestDb === null || !Storage::exists($requestDb->file_name) ) {
                $requestDb = $this->requestAndStore($url);
            }

            $data = $this->transformData($requestDb);

            Cache::put($url, $data);

            return $data;
    }

    private function transformData($requestdb) {

        $jsonDecode = Storage::get($requestdb->file_name);
        $jsonDecode = json_decode($jsonDecode, true);
        $jsonDecode = collect($jsonDecode['MRData']);

        return $jsonDecode;
    }

}
