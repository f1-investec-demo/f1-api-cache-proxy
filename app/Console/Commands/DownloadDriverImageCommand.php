<?php

namespace App\Console\Commands;

use App\Jobs\DownloadDriverImages;
use Illuminate\Console\Command;

class DownloadDriverImageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download-driver-images:f1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads F1 driver images from https://formula1.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        DownloadDriverImages::dispatch();
    }
}
