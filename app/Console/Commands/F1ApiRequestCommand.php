<?php

namespace App\Console\Commands;

use App\Models\Request as RequestDB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class F1ApiRequestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'f1-apt:fetch {segments*} {--limit=30} {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $limit;
    private $offset;
    private $segments;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->segments = $this->argument('segments');
        $this->limit = $this->argument('limit');
        $this->offset = $this->argument('offset');
        return 'asdasd';
    }


    private function buildSegments($args)
    {
        array_shift($args);

        return implode('/', $args);
    }

    private function url($part, $limit = 30, $offset = 0)
    {

//        $allowedParts = ['season', 'race', 'current', ''];
        // validation of parts parse is requred
        return "https://ergast.com/api/f1/$part.json?limit={$limit}&offset={$offset}";
    }

    private function fetch() {

        $ttlStart = now()->subSeconds(config('cacherequest.ttl'));
        $ttlEnd = now();

        $limit = $this->limit;
        $offset = $this->offset;

        $url = $this->url($this->segments, $limit, $offset);

        $requestDb = RequestDB::where('url', $url)->whereBetween('created_at', [$ttlStart, $ttlEnd])->orderBy('created_at', 'DESC')->first();

        if ($requestDb === null || !Storage::exists($requestDb->file_name)) {
            $requestDb = $this->requestAndStore($url);
        }

        return $this->transformData($requestDb);
    }


    private function transformData($requestdb)
    {

        $jsonDecode = Storage::get($requestdb->file_name);
        $jsonDecode = json_decode($jsonDecode, true);
        $jsonDecode = collect($jsonDecode['MRData']);

        return $jsonDecode;
    }


    private function requestAndStore($url)
    {
        $response = Http::get($url);

        $requestdb = new RequestDB();
        $requestdb->url = $url;
        $requestdb->type = 'json';
        $requestdb->file_name = Str::random(64) . '.json';
        $requestdb->save();

        Storage::put($requestdb->file_name, $response->body());

        return $requestdb;
    }
}
