<?php

namespace App\Jobs;


use App\Models\DriverImage;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use SebastianBergmann\CodeCoverage\Driver\Driver;

class DownloadDriverImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return string[]
     */
    public function handle()
    {
        $imagesToDownloadUrls = [
            "https://www.formula1.com/content/dam/fom-website/drivers/K/KIMRAI01_Kimi_R%C3%A4ikk%C3%B6nen/kimrai01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/L/LEWHAM01_Lewis_Hamilton/lewham01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/M/MAXVER01_Max_Verstappen/maxver01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/L/LANNOR01_Lando_Norris/lannor01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/V/VALBOT01_Valtteri_Bottas/valbot01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/S/SERPER01_Sergio_Perez/serper01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/C/CARSAI01_Carlos_Sainz/carsai01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/C/CHALEC01_Charles_Leclerc/chalec01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/D/DANRIC01_Daniel_Ricciardo/danric01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/P/PIEGAS01_Pierre_Gasly/piegas01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/E/ESTOCO01_Esteban_Ocon/estoco01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/F/FERALO01_Fernando_Alonso/feralo01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/S/SEBVET01_Sebastian_Vettel/sebvet01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/Y/YUKTSU01_Yuki_Tsunoda/yuktsu01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/L/LANSTR01_Lance_Stroll/lanstr01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/G/GEORUS01_George_Russell/georus01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/N/NICLAF01_Nicholas_Latifi/niclaf01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/A/ANTGIO01_Antonio_Giovinazzi/antgio01.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/M/MICSCH02_Mick_Schumacher/micsch02.png",
            "https://www.formula1.com/content/dam/fom-website/drivers/N/NIKMAZ01_Nikita_Mazepin/nikmaz01.png"
        ];

        foreach($imagesToDownloadUrls as $imageUrl) {
                $name = $this->extractNameFromUrl($imageUrl);
                $image = $this->downloadImage($imageUrl);

                $driverImage = DriverImage::UpdateOrCreate(
                    ['given_name' => $name[0], 'family_name' => $name[1],],
                    ['file' => $image['file_name']]
                );
        }
    }

    private function downloadImage($imageUrl): array
    {

        $urlPathInfo = pathinfo($imageUrl);

        $randomFileName = Str::random('50') . '.' . $urlPathInfo['extension'];

        $image = Http::get($imageUrl);

        $image->onError(function () use ($image) {
            throw new Exception(['status' => $image->status(), 'msg' => 'Could not load image']);
        });

        Storage::put('drivers/'. $randomFileName, $image->body());

        return ['status' => $image->status(), 'file_name' => $randomFileName];
    }

    private function extractNameFromUrl($url): \Illuminate\Support\Collection
    {
        $url = preg_match('/[A-Z]{1}\/(.*_.*)\/.*\.png/', $url, $matches);

        $nameMatch = $matches[1];
        $nameMatch = explode('_', $nameMatch);
        array_shift($nameMatch);

        $nameMatch = collect($nameMatch);

        return $nameMatch->map(function($namePart) {
            return urldecode($namePart);
        });
    }
}
