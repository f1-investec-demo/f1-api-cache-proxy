<?php

use App\Http\Controllers\RequestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/driver-images', [\App\Http\Controllers\DriverImagesController::class, 'index']);
Route::get('/countries', [\App\Http\Controllers\CountryController::class, 'index']);
Route::get('/countries/{country}', [\App\Http\Controllers\CountryController::class, 'show']);

Route::get('/proxy/{segment1}', [RequestController::class, 'fetch']);
Route::get('/proxy/{segment1}/{segment2}', [RequestController::class, 'fetch']);
Route::get('/proxy/{segment1}/{segment2}/{segment3}', [RequestController::class, 'fetch']);
Route::get('/proxy/{segment1}/{segment2}/{segment3}/{segment4}', [RequestController::class, 'fetch']);
Route::get('/proxy/{segment1}/{segment2}/{segment3}/{segment4}/{segment5}', [RequestController::class, 'fetch']);


